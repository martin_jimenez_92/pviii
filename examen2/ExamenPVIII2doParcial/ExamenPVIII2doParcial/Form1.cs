﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ExamenPVIII2doParcial
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            //string nombre = Convert.ToString(txtnombre.Text);
            //string apellido = Convert.ToString(txtapellido.Text);
        }

        private void btnagregar_Click(object sender, EventArgs e)
        {
            StreamWriter agregar = new StreamWriter(@"C:\Users\Martin Jimenez\UNEDL\pviii\examen2\Examen.txt",true);
            try
            {
                agregar.WriteLine("Nombre: " + txtnombre.Text);
                agregar.WriteLine("Apellido: " + txtapellido.Text);
                agregar.WriteLine("\n");
                agregar.Close();
            }
            catch
            {
                MessageBox.Show("Error");
            }
            agregar.Close();
            txtnombre.Text = "";
            txtapellido.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StreamReader mostrar = new StreamReader(@"C:\Users\Martin Jimenez\UNEDL\pviii\examen2\Examen.txt");
            string leer;
            try
            {
                leer = mostrar.ReadLine();
                while(leer != null)
                {
                    richTextBox1.AppendText(leer + "\n");
                    leer = mostrar.ReadLine();
                }
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }
    }
}
